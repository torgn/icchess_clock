/*************************************************************************
 * File: ChessClockService.java
 * 
 * Derived from ChessClock.java.
 * 
 * Created: 1/13/2016
 * 
 * Author: Tor Garmann Naerland 
 * 
 *************************************************************************
 *
 *   This file is part of ICChess Clock (ICCC).
 *    
 *   ICCC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ICCC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with ICCC.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

package org.bitbucket.torgn.icchess_clock;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.widget.Toast;

import java.lang.ref.WeakReference;




/**-----------------------------------
 *  	  CHESS CLOCK SERVICE
 *-----------------------------------*/

public class ChessClockService extends Service {
	private Looper mServiceLooper;
	private IncomingHandler mIncomingHandler;

	/** Command to the service to control the clock */
	static final int TAP_PLAYER1_CLOCK = 1;
	static final int TAP_PLAYER2_CLOCK = 2;
	static final int START_CLOCK = 3;
	static final int TOGGLE_PAUSE = 4;
	static final int RESET_CLOCK = 5;
	static final int PAUSE_CLOCK = 6;
	static final int MSG_REGISTER_CLIENT = 7;
	static final int TIME_PLAYER1 = 8;
	static final int TIME_PLAYER2 = 9;

	/**-----------------------------------
	 *            CONSTANTS
	 *-----------------------------------*/
	/** Version info and debug tag constants */
	public static final String TAG = "ChessClockService";
	public static final String V_MAJOR = "1";
	public static final String V_MINOR = "2";
	public static final String V_MINI = "0";

	/** Time control values */
	private static String NO_DELAY = "None";
	private static String FISCHER = "Fischer";
	private static String BRONSTEIN = "Bronstein";

	/**-----------------------------------
	 *     CHESSCLOCK CLASS MEMBERS
	 *-----------------------------------*/
	/** Objects/Classes */
	private Handler myHandler = new Handler();
	private String delay = NO_DELAY;
	private String alertTone;
	private Ringtone ringtone = null;

	/** ints/longs */
	private int time;
	private int b_delay;
	private long t_P1;
	private long t_P2;
	private int delay_time;
	private int onTheClock = 0;
	private int savedOTC = 0;

	/** booleans */
	private boolean haptic = false;
	private boolean timeup = false;
	private boolean prefmenu = false;
	private boolean delayed = false;
	private boolean hapticChange = false;

	/** Messenger for client replies */
	Messenger mClient = null;


//	  @Override
//	  public void onCreate() {
//	    // Start up the thread running the service.  Note that we create a
//	    // separate thread because the service normally runs in the process's
//	    // main thread, which we don't want to block.  We also make it
//	    // background priority so CPU-intensive work will not disrupt our UI.
//	    HandlerThread thread = new HandlerThread("ServiceStartArguments",
//	            Process.THREAD_PRIORITY_BACKGROUND);
//	    thread.start();
//
//	    // Get the HandlerThread's Looper and use it for our Handler
//	    mServiceLooper = thread.getLooper();
//	    mIncomingHandler = new IncomingHandler(mServiceLooper);
//	  }


	/**
	 * Handler of incoming messages from client.
	 */
	private static class IncomingHandler extends Handler {
		private final WeakReference<ChessClockService> mService;

		IncomingHandler(ChessClockService service) {
			mService = new WeakReference<ChessClockService>(service);
		}

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case TAP_PLAYER1_CLOCK:
					mService.get().P1Click();
					break;
				case TAP_PLAYER2_CLOCK:
					mService.get().P2Click();
					break;
				case START_CLOCK:
					mService.get().SetUpGame();
					mService.get().onTheClock = 0;
					mService.get().P2Click();
					break;
				case PAUSE_CLOCK:
					mService.get().PauseGame();
					break;
				case TOGGLE_PAUSE:
					mService.get().PauseToggle();
					break;
				case RESET_CLOCK:
					mService.get().SetUpGame(); // npe here
					mService.get().onTheClock = 0;
					break;
				case MSG_REGISTER_CLIENT:
					mService.get().mClient = msg.replyTo;
					/** Format and post the clocks */
					mService.get().sendMessage( FormatTime(mService.get().t_P1), TIME_PLAYER1);
					mService.get().sendMessage( FormatTime(mService.get().t_P2), TIME_PLAYER2);
					break;
				case TIME_PLAYER1:
					mService.get().t_P1 = (long) msg.arg1;
					//Log.d(TAG, "setting time P1: "+ FormatTime(mService.get().t_P1));
					break;
				case TIME_PLAYER2:
					mService.get().t_P2 = (long) msg.arg1;
				default:
					super.handleMessage(msg);
			}
		}
	}


//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
//        Log.d("Chess clock service", "service starting");
//        // For each start request, send a message to start a job and deliver the
//        // start ID so we know which request we're stopping when we finish the job
//        Message msg = mIncomingHandler.obtainMessage();
//        msg.arg1 = startId;
//        mIncomingHandler.sendMessage(msg);
//
//        // If we get killed, after returning from here, restart
//        return START_STICKY;
//    }

	@Override
	public void onDestroy() {
		Toast.makeText(this, "Chess clock service finished.", Toast.LENGTH_SHORT).show();
	}


	/**
	 * Target we publish for client to send messages to IncomingHandler.
	 */
	final Messenger mMessenger = new Messenger( new IncomingHandler(this));

	/**
	 * When binding to the service, we return an interface to our messenger
	 * for sending messages to the service.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		SetUpGame();
		Toast.makeText(getApplicationContext(), "Chess clock service started.", Toast.LENGTH_SHORT).show();
		return mMessenger.getBinder();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		//Log.v("ChessClockService", "in onUnbind: trying to call stopself()");
		stopSelf();
		return false;
	}

	private void sendMessage(String msg, int what) {
		if ( mClient == null ) {
			return;
		}
		try {
			Bundle msgBundle = new Bundle();
			msgBundle.putString("msg", msg);
			Message msgObj = Message.obtain(null, what);
			msgObj.setData(msgBundle);
			mClient.send(msgObj);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Called when P1ClickHandler registers a click/touch event */
	private void P1Click() {

		/** Check if this is valid (i.e. if our time is running */
		if ( onTheClock == 1 )
			return;

		/**
		 * Register that our time is running now
		 * and that we haven't yet received our delay
		 */
		onTheClock = 1;
		if ( savedOTC == 0 ) {
			delayed = false;
		} else {
			savedOTC = 0;
		}


		if ( delay.equals(BRONSTEIN) ) {

			int secondsLeft = (int) (t_P2 / 1000);
			int minutesLeft = secondsLeft / 60;
			secondsLeft     = secondsLeft % 60;

			secondsLeft += 1;
			if ( secondsLeft == 60 ) {
				minutesLeft += 1;
				secondsLeft = 0;
			} else if ( t_P2 == 0 ) {
				secondsLeft = 0;
			} else if ( t_P2 == time * 60000 ) {
				secondsLeft -= 1;
			}
			if (secondsLeft < 10) {
				sendMessage( "" + minutesLeft + ":0" + secondsLeft, TAP_PLAYER1_CLOCK);
			} else {
				sendMessage( "" + minutesLeft + ":" + secondsLeft, TAP_PLAYER1_CLOCK);
			}
		}


		/**
		 * Unregister the handler from player 2's clock and
		 * create a new one which we register with this clock.
		 */
		myHandler.removeCallbacks(mUpdateTimeTask);
		myHandler.removeCallbacks(mUpdateTimeTask2);
		myHandler.postDelayed(mUpdateTimeTask, 100);

		return;
	}

	/** Handles the "tick" event for Player 1's clock */
	private Runnable mUpdateTimeTask = new Runnable() {
		public void run() {
			String delay_string = "";

			/** Check for delays and apply them */
			if ( delay.equals(FISCHER) && !delayed ) {
				delayed = true;
				t_P1 += delay_time * 1000;
			} else if ( delay.equals(BRONSTEIN) && !delayed ) {
				delayed = true;
				b_delay = delay_time * 1000; //Deduct the first .1s;
				t_P1 += 100; //We'll deduct this again shortly
				delay_string = "+" + (b_delay / 1000 );
			} else if ( delay.equals(BRONSTEIN) && delayed ) {
				if ( b_delay > 0 ) {
					b_delay -= 100;
					t_P1 += 100;
				}
				if (b_delay > 0 ) {
					delay_string = "+" + ( ( b_delay / 1000 ) + 1 );
				}
			}

			/** Deduct 0.1s from P1's clock */
			t_P1 -= 100;
			long timeLeft = t_P1;

			/** Format for display purposes */
			int secondsLeft = (int) (timeLeft / 1000);
			int minutesLeft = secondsLeft / 60;
			secondsLeft     = secondsLeft % 60;

			secondsLeft += 1;
			if ( secondsLeft == 60 ) {
				minutesLeft += 1;
				secondsLeft = 0;
			} else if ( timeLeft == 0 ) {
				secondsLeft = 0;
			} else if ( timeLeft == time * 60000 ) {
				secondsLeft -= 1;
			}

			/** Did we run out of time? */
			if ( timeLeft == 0 ) {
				timeup = true;

//				Uri uri = Uri.parse(alertTone);
//				ringtone = RingtoneManager.getRingtone(getBaseContext(), uri);
//				if ( null != ringtone ) {
//					ringtone.play();
//				}

				/** stop the clock service */
				myHandler.removeCallbacks(mUpdateTimeTask2); //myHandler.removeCallbacks(mUpdateTimeTask2); <- guessing this was a bug: Task2 instead of 1
				//Log.d(TAG, "calling stopSelt();");
				sendMessage( "0:00", TIME_PLAYER1);
				return;
			}


			/** Send the time, omitting leading 0's for times < 10 minutes */
			if (secondsLeft < 10) {
				sendMessage( minutesLeft + ":0" + secondsLeft + delay_string, TIME_PLAYER1);
			} else {
				sendMessage( minutesLeft + ":" + secondsLeft + delay_string , TIME_PLAYER1);
			}

			/** Re-post the handler so it fires in another 0.1s */
			myHandler.postDelayed(this, 100);
		}
	};

	/** Called when P2ClickHandler registers a click/touch event */
	private void P2Click() {

		/** Check if this is valid (i.e. if our time is running */
		if ( onTheClock == 2 )
			return;

		/**
		 * Register that our time is running now
		 * and that we haven't yet received our delay
		 */
		onTheClock = 2;
		if ( savedOTC == 0 ) {
			delayed = false;
		} else {
			savedOTC = 0;
		}


		if ( delay.equals(BRONSTEIN) ) {

			int secondsLeft = (int) (t_P1 / 1000);
			int minutesLeft = secondsLeft / 60;
			secondsLeft     = secondsLeft % 60;

			secondsLeft += 1;
			if ( secondsLeft == 60 ) {
				minutesLeft += 1;
				secondsLeft = 0;
			} else if ( t_P1 == 0 ) {
				secondsLeft = 0;
			} else if ( t_P1 == time * 60000 ) {
				secondsLeft -= 1;
			}
			if (secondsLeft < 10) {
				sendMessage( minutesLeft + ":0" + secondsLeft, TAP_PLAYER2_CLOCK);
			} else {
				sendMessage( minutesLeft + ":" + secondsLeft, TAP_PLAYER2_CLOCK);
			}
		}


		/**
		 * Unregister the handler from player 1's clock and
		 * create a new one which we register with this clock.
		 */
		myHandler.removeCallbacks(mUpdateTimeTask);
		myHandler.removeCallbacks(mUpdateTimeTask2);
		myHandler.postDelayed(mUpdateTimeTask2, 100);

		return;
	}

	/** Handles the "tick" event for Player 2's clock */
	private Runnable mUpdateTimeTask2 = new Runnable() {
		public void run() {
			String delay_string = "";

			/** Check for delays and apply them */
			if ( delay.equals(FISCHER) && !delayed ) {
				delayed = true;
				t_P2 += delay_time * 1000;
			} else if ( delay.equals(BRONSTEIN) && !delayed ) {
				delayed = true;
				b_delay = delay_time * 1000; //Deduct the first .1s;
				t_P2 += 100; //We'll deduct this again shortly
				delay_string = "+" + ( b_delay / 1000 );
			} else if ( delay.equals(BRONSTEIN) && delayed ) {
				if ( b_delay > 0 ) {
					b_delay -= 100;
					t_P2 += 100;
				}
				if (b_delay > 0 ) {
					delay_string = "+" + ( ( b_delay / 1000 ) + 1 );
				}
			}

			/** Deduct 0.1s from P2's clock */
			t_P2 -= 100;
			long timeLeft = t_P2;

			/** Format for display purposes */
			int secondsLeft = (int) (timeLeft / 1000);
			int minutesLeft = secondsLeft / 60;
			secondsLeft     = secondsLeft % 60;

			secondsLeft += 1;
			if ( secondsLeft == 60 ) {
				minutesLeft += 1;
				secondsLeft = 0;
			} else if ( timeLeft == 0 ) {
				secondsLeft = 0;
			} else if ( timeLeft == time * 60000 ) {
				secondsLeft -= 1;
			}

			/** Did we run out of time? */
			if ( timeLeft == 0 ) {
				timeup = true;

//				Uri uri = Uri.parse(alertTone);
//				ringtone = RingtoneManager.getRingtone(getBaseContext(), uri);
//				if ( null != ringtone ) {
//					ringtone.play();
//				}
				/** stop the clock service */
				myHandler.removeCallbacks(mUpdateTimeTask2);
				//Log.d(TAG, "calling stopSelt();");
				sendMessage( "0:00", TIME_PLAYER2);
				return;
			}

			/** Send the time, omitting leading 0's for times < 10 minutes */
			if (secondsLeft < 10) {
				sendMessage( minutesLeft + ":0" + secondsLeft + delay_string, TIME_PLAYER2);
			} else {
				sendMessage( minutesLeft + ":" + secondsLeft + delay_string, TIME_PLAYER2);
			}

			/** Re-post the handler so it fires in another 0.1s */
			myHandler.postDelayed(this, 100);
		}
	};

	/**
	 * Pauses both clocks. This is called when the options
	 * menu is opened, since the game needs to pause
	 * but not un-pause, whereas PauseToggle() will switch
	 * back and forth between the two.
	 *  */
	private void PauseGame() {

		/** Save the currently running clock, then pause */
		if ( ( onTheClock != 0 ) && ( !timeup ) ) {
			savedOTC = onTheClock;
			onTheClock = 0;

			myHandler.removeCallbacks(mUpdateTimeTask);
			myHandler.removeCallbacks(mUpdateTimeTask2);
		}
	}

	/** Called when the pause button is clicked */
	private void PauseToggle() {

		/** Figure out if we need to pause or unpause */
		if ( onTheClock != 0 ) {
			savedOTC = onTheClock;
			onTheClock = 0;

			myHandler.removeCallbacks(mUpdateTimeTask);
			myHandler.removeCallbacks(mUpdateTimeTask2);

		} else {
			//Log.v(TAG, "Info: Unpausing.");
			if ( savedOTC == 1 ) {
				P1Click();
			} else if ( savedOTC == 2 ) {
				P2Click();
			} else {
				return;
			}
		}
	}

	/** Set up (or refresh) all game parameters */
	private void SetUpGame() {
		/** Load all stored preferences */
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		/** Take care of a haptic change if needed */
		haptic = prefs.getBoolean("prefHaptic", false);

		if (hapticChange)
		{
			/**
			 * We're just changing haptic feedback on this run through,
			 * don't reload everything else!
			 */
			hapticChange = false;
			return;
		}

		delay = prefs.getString("prefDelay","None");
		if ( delay.equals("")) {
			delay = "None";
			Editor e = prefs.edit();
			e.putString("prefDelay", "None");
			e.commit();
		}

		try {
			time = Integer.parseInt( prefs.getString("prefTime", "10") );
			//Log.d( TAG, "time: " + time);
		} catch (Exception ex) {
			//Log.d( TAG, "Exception while parsing prefTime");
			time = 10;
			Editor e = prefs.edit();
			e.putString("prefTime", "10");
			e.commit();
		}

		try {
			delay_time = Integer.parseInt( prefs.getString("prefDelayTime", "0") );
		} catch (Exception ex) {
			delay_time = 0;
			Editor e = prefs.edit();
			e.putString("prefDelayTime", "0");
			e.commit();
		}

		alertTone = prefs.getString("prefAlertSound", Settings.System.DEFAULT_RINGTONE_URI.toString());
		if (alertTone.equals("")) {
			alertTone = Settings.System.DEFAULT_RINGTONE_URI.toString();
			Editor e = prefs.edit();
			e.putString("prefAlertSound", alertTone);
			e.commit();
		}

		Uri uri = Uri.parse(alertTone);
		ringtone = RingtoneManager.getRingtone(getBaseContext(), uri);

		/** Set time equal to minutes * ms per minute */
		t_P1 = time * 60000;
		t_P2 = time * 60000;


		/** Format and post the clocks */
		sendMessage( FormatTime(t_P1), TIME_PLAYER1);
		sendMessage( FormatTime(t_P2), TIME_PLAYER2);
	}

	/**
	 * Formats the provided time to a readable string
	 * @param time - time to format
	 * @return str_time - formatted time (String)
	 */
	private static String FormatTime(long time) {
		int secondsLeft = (int)time / 1000;
		int minutesLeft = secondsLeft / 60;
		secondsLeft     = secondsLeft % 60;

		String str_time;

		if (secondsLeft < 10) {
			str_time = "" + minutesLeft + ":0" + secondsLeft;
		} else {
			str_time = "" + minutesLeft + ":" + secondsLeft;
		}

		return str_time;
	}
}