/*************************************************************************
 * File: Prefs.java
 * 
 * Implements the Preferences dialog.
 * 
 * Created: 6/23/2010
 * 
 * Author: Carter Dewey
 * 
 * Modified: 1/13/2016 by Tor Garmann Naerland
 *************************************************************************
 *
 *   This file is part of Simple Chess Clock (SCC).
 *    
 *   SCC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   SCC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with SCC.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/
package org.bitbucket.torgn.icchess_clock;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
 
public class Prefs extends PreferenceActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		setContentView(R.layout.pref_buttons);
		
		boolean withButtons = false;
		try {
			getIntent().getExtras().getBoolean("buttons");
		} catch( NullPointerException npe) {
			// a quick and dirty work around. Not pretty, but apparently does the trick.
			withButtons = true; 
		}
		
	    
	    if ( withButtons ) {
	    	// If started from the chess clock service we provide buttons.

	    	Button apply = (Button) findViewById(R.id.buttonApply);
	    	Button off = (Button) findViewById(R.id.buttonOff);
	    	apply.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View v) {
					setResult(RESULT_OK);
					finish();
				}
			});
	    	
	    	off.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View v) {
					setResult(RESULT_FIRST_USER);
					finish();
				}
			});
	    	
	    	apply.setVisibility(View.VISIBLE);
	    	off.setVisibility(View.VISIBLE);

	    }	    
	}
}
