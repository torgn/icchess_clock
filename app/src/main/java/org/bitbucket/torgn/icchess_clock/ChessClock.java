/*************************************************************************
 * File: ChessClock.java
 * 
 * Implements the main form/class for Chess Clock.
 * 
 * Created: 6/22/2010
 * 
 * Author: Carter Dewey
 * 
 * Modified: 1/13/2016 by Tor Garmann Naerland
 *************************************************************************
 *
 *   This file is part of Simple Chess Clock (SCC).
 *    
 *   SCC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   SCC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with SCC.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

package org.bitbucket.torgn.icchess_clock;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class ChessClock extends Activity {

	/**-----------------------------------
	 *        BLUETOOTH RELATED
	 *-----------------------------------*/

	private static String mConnectedDeviceName = null;  // Name of the connected device
	//private static ArrayAdapter<String> mConversationArrayAdapter; //Array adapter for the conversation thread
	private static StringBuffer mOutStringBuffer; //String buffer for outgoing messages
	private static BluetoothAdapter mBluetoothAdapter = null;  //Local Bluetooth adapter
	private BluetoothMessagingService mBTmessagingService = null;  // Member object for the text exchange services
	// Intent request codes
	private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
	private static final int REQUEST_ENABLE_BT = 3;

	/**-----------------------------------
	 *            CONSTANTS
	 *-----------------------------------*/
	/** Version info and debug tag constants */
	public static final String TAG = "INFO";
	public static final String V_MAJOR = "1";
	public static final String V_MINOR = "2";
	public static final String V_MINI = "0";

	/** Constants for the dialog windows */
	private static final int SETTINGS = 0;
	private static final int RESET = 1;
	private static final int ABOUT = 2;

	/** Time control values */
	private static String NO_DELAY = "None";
	private static String FISCHER = "Fischer";
	private static String BRONSTEIN = "Bronstein";

	/**-----------------------------------
	 *     CHESSCLOCK CLASS MEMBERS
	 *-----------------------------------*/
	/** Objects/Classes */
	private Handler myHandler = new Handler();
	private DialogFactory DF = new DialogFactory();
	//private PowerManager pm;
	//private WakeLock wl;
	private String delay = NO_DELAY;
	private String alertTone;
	private Ringtone ringtone = null;
	private ImageButton buttonSettings;
	private ImageButton buttonReset;
	private ImageButton buttonBT;
	private ImageButton buttonAbout;

	/** ints/longs */
	private int time;
	private int b_delay;
	private long t_P1;
	private long t_P2;
	private int delay_time;
	private int onTheClock = 0;
	private int savedOTC = 0;

	/** booleans */
	private boolean haptic = false;
	private boolean blink = false;
	private boolean timeup = false;
	private boolean prefmenu = false;
	private boolean delayed = false;
	private boolean hapticChange = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		if ( intent.getAction() == "com.chessclock.android.START_CHESS_CLOCK_SERVICE" ) {
			// start service
			//Intent serviceIntent = new Intent("com.chessclock.android.ChessClockService");
			//this.startService(serviceIntent);
			// Open Preferences
			showPrefs( true );
			// finish.
			finish();
		}

		/** Get rid of the status bar */
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN |
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );

//        /** Create a PowerManager object so we can get the wakelock */
//        pm = (PowerManager) getSystemService(ChessClock.POWER_SERVICE);
//        wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "ChessWakeLock");

		setContentView(R.layout.main);

		this.buttonBT = (ImageButton) findViewById(R.id.buttonBluetooth);
		this.buttonAbout = (ImageButton) findViewById(R.id.buttonAbout);
		this.buttonSettings = (ImageButton) findViewById(R.id.button1);
		this.buttonReset = (ImageButton) findViewById(R.id.button2);

		buttonBT.setOnClickListener(BTListener);
		buttonAbout.setOnClickListener(AboutListener);
		buttonSettings.setOnClickListener(SettingsListener);
		buttonReset.setOnClickListener(ResetListener);

		/** Get local Bluetooth adapter */
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
			buttonBT.setVisibility(View.GONE);
		}

		SetUpGame();
	}

	@Override
	public void onPause() {
//    	if ( wl.isHeld() ) {
//    		wl.release();
//    	}

		if (null != ringtone) {
			if ( ringtone.isPlaying() ) {
				ringtone.stop();
			}
		}

		PauseGame();
		super.onPause();
	}

	@Override
	public void onResume() {
		/** Get the wakelock */
		//wl.acquire();

		if (null != ringtone) {
			if ( ringtone.isPlaying() ) {
				ringtone.stop();
			}
		}
		super.onResume();

		/** Resume the Bluetooth service if not null*/
		if ( mBTmessagingService != null ) {
			if ( mBTmessagingService.getState() == BluetoothMessagingService.STATE_NONE )
			{
				mBTmessagingService.start();
			}
		}
	}

	@Override
	public void onDestroy() {
//    	if ( wl.isHeld() ) {
//    		wl.release();
//    	}

		if ( mBTmessagingService != null ) {
			mBTmessagingService.stop();
		}


		if (null != ringtone) {
			if ( ringtone.isPlaying() ) {
				ringtone.stop();
			}
		}
		super.onDestroy();
	}

	/**
	 * Formats the provided time to a readable string
	 * @param time - time to format
	 * @return str_time - formatted time (String)
	 */
	private String FormatTime(long time) {
		int secondsLeft = (int)time / 1000;
		int minutesLeft = secondsLeft / 60;
		secondsLeft     = secondsLeft % 60;

		String str_time;

		if (secondsLeft < 10) {
			str_time = "" + minutesLeft + ":0" + secondsLeft;
		} else {
			str_time = "" + minutesLeft + ":" + secondsLeft;
		}

		return str_time;
	}

	public boolean onPrepareOptionsMenu(Menu menu) {
		prefmenu = true;
		if ( null != ringtone ) {
			if ( ringtone.isPlaying() ) {
				ringtone.stop();
			}
		}
		PauseGame();
		return true;
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		//getMenuInflater().inflate(R.menu.bluetooth_chat, menu);
		menu.add(0, SETTINGS, 0, "Settings").setIcon(R.drawable.settings);
		menu.add(0, RESET, 0, "Reset Clocks").setIcon(R.drawable.refresh);
		menu.add(0, ABOUT, 0, "About").setIcon(R.drawable.about);

		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch( item.getItemId() ) {

			case R.id.discoverable: {
				// Ensure this device is discoverable by others
				ensureDiscoverable();
				return true;
			}
			case SETTINGS:
				showPrefs(false);
				return true; // settings is now opened with a button instead.
			case RESET:
				showDialog(RESET);
				return true;
			case ABOUT:
				showDialog(ABOUT);
				return true;
		}

		return false;
	}


	/**
	 * Makes this device discoverable.
	 */
	private void ensureDiscoverable() {
		if (mBluetoothAdapter.getScanMode() !=
				BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}


	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case REQUEST_CONNECT_DEVICE_SECURE:
				// When DeviceListActivity returns with a device to connect
				if (resultCode == Activity.RESULT_OK) {
					connectDevice(data, true);
				}
				break;
			case REQUEST_ENABLE_BT:
				// When the request to enable Bluetooth returns
				if (resultCode == Activity.RESULT_OK) {
					// Bluetooth is now enabled, so set up a chat session
					setupComLink();

					// Launch the DeviceListActivity to see devices and do scan
					Intent serverIntent = new Intent(this, BluetoothDeviceListActivity.class);
					startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);

				} else {
					// User did not enable Bluetooth or an error occurred
					//Log.d(TAG, "BT not enabled");
					Toast.makeText( this, "Bluetooth was not enabled.",
							Toast.LENGTH_SHORT).show();
					//getActivity().finish();
				}
		}
	}

	public void onWindowFocusChanged(boolean b) {
		if ( !prefmenu ) {
			CheckForNewPrefs();
		} else {
			prefmenu = false;
		}
	}

	protected Dialog onCreateDialog(int id) {
		Dialog dialog = new Dialog(this);
		switch ( id ) {
			case ABOUT:
				dialog = DF.AboutDialog(this, V_MAJOR, V_MINOR, V_MINI);
				break;
			case RESET:
				dialog = ResetDialog();
				break;
		}

		return dialog;
	}

	/** Click handler for player 1's clock. */
	public View.OnClickListener P1ClickHandler = new View.OnClickListener() {
		public void onClick(View v) {
			P1Click(true);
		}
	};

	/** Click handler for player 2's clock */
	public View.OnClickListener P2ClickHandler = new View.OnClickListener() {
		public void onClick(View v) {
			ImageButton pause = (ImageButton)findViewById(R.id.Pause);
			pause.setOnClickListener(PauseListener); // strictly not neccessary to call it every time. It is merely for making the play button function as a start button initially.
			P2Click(true);
		}
	};

	/** Click handler for the pause button */
	public View.OnClickListener PauseListener = new View.OnClickListener() {
		public void onClick(View v) {
			PauseToggle();
		}
	};

	public View.OnClickListener BTListener = new View.OnClickListener() {

		public void onClick(View v) {
			if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
				Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
				return;
				// Otherwise, setup the chat session
			} else if (mBTmessagingService == null) {
				setupComLink();
			}

			// Launch the DeviceListActivity to see devices and do scan
			Intent serverIntent = new Intent(getApplicationContext(), BluetoothDeviceListActivity.class);
			startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
		}

	};

	public View.OnClickListener SettingsListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			showPrefs(false);
			//Log.v(TAG, "INFO: Trying to create Preferences screen");
		}
	};

	public View.OnClickListener ResetListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			showDialog(RESET)		;
		}
	};
	public View.OnClickListener AboutListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			showDialog(ABOUT);
		}
	};
	/** Starts the Preferences menu intent
	 * @param  */
	private void showPrefs(boolean withButtons) {
		Intent prefsActivity = new Intent(ChessClock.this, Prefs.class);
		Bundle b = new Bundle();
		b.putBoolean("buttons", withButtons?true:false);
		prefsActivity.putExtras(b);
		startActivity(prefsActivity);
	}

	/**
	 * Checks for changes to the current preferences. We only want
	 * to re-create the game if something has been changed, so we
	 * check for differences any time onWindowFocusChanged() is called.
	 */
	public void CheckForNewPrefs() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		alertTone = prefs.getString("prefAlertSound", Settings.System.DEFAULT_RINGTONE_URI.toString());

		/** Check for a new delay style */
		String new_delay = prefs.getString("prefDelay","None");
		if (new_delay.equals("")) {
			new_delay = "None";
			SharedPreferences.Editor e = prefs.edit();
			e.putString("prefDelay", "None");
			e.commit();
		}

		if ( new_delay != delay ) {
			SetUpGame();
		}

		/** Check for a new game time setting */
		int new_time;

		try {
			new_time = Integer.parseInt( prefs.getString("prefTime", "10") );
		} catch (Exception ex) {
			new_time = 10;
			SharedPreferences.Editor e = prefs.edit();
			e.putString("prefTime", "10");
			e.commit();
		}

		if ( new_time != time ) {
			SetUpGame();
		}

		/** Check for a new delay time */
		int new_delay_time;
		try {
			new_delay_time = Integer.parseInt( prefs.getString("prefDelayTime", "0" ) );
		} catch (Exception ex) {
			new_delay_time = 0;
			SharedPreferences.Editor e = prefs.edit();
			e.putString("prefDelayTime", "0");
			e.commit();
		}

		if ( new_delay_time != delay_time ) {
			SetUpGame();
		}

		boolean new_haptic = prefs.getBoolean("prefHaptic", false);
		if ( new_haptic != haptic ) {
			// No reason to reload the clocks for this one
			hapticChange = true;
			SetUpGame();
		}
	}

	/** Creates and displays the "Reset Clocks" alert dialog */
	private Dialog ResetDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Reset both clocks?")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						SetUpGame();
						onTheClock = 0;
						dialog.dismiss();
					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();

		return alert;
	}

	/** Called when P1ClickHandler registers a click/touch event */
	private boolean P1Click(boolean notify) {
		Button p1_button = (Button)findViewById(R.id.Player1);
		Button p2_button = (Button)findViewById(R.id.Player2);

		p1_button.performHapticFeedback(1);

//		/** Un-dim the screen */
//		PowerManager pm = (PowerManager)getBaseContext().getSystemService(
//                Context.POWER_SERVICE);
//		pm.userActivity(1, true);

		/** Check if this is valid (i.e. if our time is running */
		if ( onTheClock == 1 )
			return false;

		/**
		 * Register that our time is running now
		 * and that we haven't yet received our delay
		 */
		onTheClock = 1;
		if ( savedOTC == 0 ) {
			delayed = false;
		} else {
			savedOTC = 0;
		}

		/**
		 * Make the other player's button green and our
		 * button and the pause button gray.
		 */
		p2_button.setBackgroundResource(R.drawable.rounded_corners_bg2);//p2_button.setBackgroundColor(Color.GREEN);
		p1_button.setBackgroundResource(R.drawable.rounded_corners_bg);//p1_button.setBackgroundColor(Color.LTGRAY);

		TextView p2 = (TextView)findViewById(R.id.t_Player2);
		if ( delay.equals(BRONSTEIN) ) {

			int secondsLeft = (int) (t_P2 / 1000);
			int minutesLeft = secondsLeft / 60;
			secondsLeft     = secondsLeft % 60;

			secondsLeft += 1;
			if ( secondsLeft == 60 ) {
				minutesLeft += 1;
				secondsLeft = 0;
			} else if ( t_P2 == 0 ) {
				secondsLeft = 0;
			} else if ( t_P2 == time * 60000 ) {
				secondsLeft -= 1;
			}
			if (secondsLeft < 10) {
				p2.setText("" + minutesLeft + ":0" + secondsLeft);
			} else {
				p2.setText("" + minutesLeft + ":" + secondsLeft);
			}
		}

		ImageButton pp = (ImageButton)findViewById(R.id.Pause);
		pp.setImageResource(R.drawable.ic_pause_white_24dp);//pp.setBackgroundColor(Color.LTGRAY);

		/**
		 * Unregister the handler from player 2's clock and
		 * create a new one which we register with this clock.
		 */
		myHandler.removeCallbacks(mUpdateTimeTask);
		myHandler.removeCallbacks(mUpdateTimeTask2);
		myHandler.postDelayed(mUpdateTimeTask, 100);

		//sendMessage( "{ p2_time: " + p2.getText().toString() + "}");
		if( notify )
			sendMessage("{ \"tick\":false,\"player\":1, \"time\": \"" + p2.getText().toString() + "\"}");
		return true;
	}

	/** Handles the "tick" event for Player 1's clock */
	private Runnable mUpdateTimeTask = new Runnable() {
		public void run() {
			TextView p1 = (TextView)findViewById(R.id.t_Player1);
			String delay_string = "";

			/** Check for delays and apply them */
			if ( delay.equals(FISCHER) && !delayed ) {
				delayed = true;
				t_P1 += delay_time * 1000;
			} else if ( delay.equals(BRONSTEIN) && !delayed ) {
				delayed = true;
				b_delay = delay_time * 1000; //Deduct the first .1s;
				t_P1 += 100; //We'll deduct this again shortly
				delay_string = "+" + (b_delay / 1000 );
			} else if ( delay.equals(BRONSTEIN) && delayed ) {
				if ( b_delay > 0 ) {
					b_delay -= 100;
					t_P1 += 100;
				}
				if (b_delay > 0 ) {
					delay_string = "+" + ( ( b_delay / 1000 ) + 1 );
				}
			}

			/** Deduct 0.1s from P1's clock */
			t_P1 -= 100;
			long timeLeft = t_P1;

			/** Format for display purposes */
			int secondsLeft = (int) (timeLeft / 1000);
			int minutesLeft = secondsLeft / 60;
			secondsLeft     = secondsLeft % 60;

			secondsLeft += 1;
			if ( secondsLeft == 60 ) {
				minutesLeft += 1;
				secondsLeft = 0;
			} else if ( timeLeft == 0 ) {
				secondsLeft = 0;
			} else if ( timeLeft == time * 60000 ) {
				secondsLeft -= 1;
			}

			/** Did we run out of time? */
			if ( timeLeft == 0 ) {
				timeup = true;
				Button b1 = (Button)findViewById(R.id.Player1);
				Button b2 = (Button)findViewById(R.id.Player2);
				ImageButton pp = (ImageButton)findViewById(R.id.Pause);

				/** Set P1's button and clock text to red */
				p1.setTextColor(Color.RED);
				b2.setBackgroundResource(R.drawable.button_bg_red);
				p1.setOnClickListener(SettingsListener); // a quick way, and the most intuitive one, without the hassle of adding another button, to shut up alarm

				b1.setClickable(false);
				b2.setClickable(false);
				pp.setClickable(false);

				Uri uri = Uri.parse(alertTone);
				ringtone = RingtoneManager.getRingtone(getBaseContext(), uri);
				if ( null != ringtone ) {
					ringtone.play();
				}

				/** Blink the clock display */
				myHandler.removeCallbacks(mUpdateTimeTask2);
				myHandler.postDelayed(Blink, 500);

				sendMessage("{ \"tick\":true,\"player\":0, \"time\": \"" + "0:00" + "\"}");

				return;

			}

			/** Color clock yellow if we're under 1 minute */
			if ( timeLeft < 60000 ) {
				p1.setTextColor(Color.rgb(255,150,0));
			} else {
				p1.setTextColor(Color.LTGRAY);
			}

			/** Display the time, omitting leading 0's for times < 10 minutes */
			if (secondsLeft < 10) {
				p1.setText("" + minutesLeft + ":0" + secondsLeft + delay_string);
			} else {
				p1.setText("" + minutesLeft + ":" + secondsLeft + delay_string);
			}

			/** Re-post the handler so it fires in another 0.1s */
			myHandler.postDelayed(this, 100);

			sendMessage("{ \"tick\":true,\"player\":0, \"time\": \"" + p1.getText().toString() + "\"}");

		}
	};

	/** Called when P2ClickHandler registers a click/touch event */
	private boolean P2Click(boolean notify) {
		Button p1_button = (Button)findViewById(R.id.Player1);
		Button p2_button = (Button)findViewById(R.id.Player2);

		p2_button.performHapticFeedback(1);

//		/** Un-dim the screen */
//		PowerManager pm = (PowerManager)getBaseContext().getSystemService(
//                Context.POWER_SERVICE);
//		pm.userActivity((long) 1, true);

		/** Check if this is valid (i.e. if our time is running */
		if ( onTheClock == 2 )
			return false;

		/**
		 * Register that our time is running now
		 * and that we haven't yet received our delay
		 */
		onTheClock = 2;
		if ( savedOTC == 0 ) {
			delayed = false;
		} else {
			savedOTC = 0;
		}

		/**
		 * Make the other player's button green and our
		 * button and the pause button gray.
		 */
		p1_button.setBackgroundResource(R.drawable.rounded_corners_bg2);//p1_button.setBackgroundColor(Color.GREEN);
		p2_button.setBackgroundResource(R.drawable.rounded_corners_bg);//p2_button.setBackgroundColor(Color.LTGRAY);

		TextView p1 = (TextView)findViewById(R.id.t_Player1);

		if ( delay.equals(BRONSTEIN) ) {

			int secondsLeft = (int) (t_P1 / 1000);
			int minutesLeft = secondsLeft / 60;
			secondsLeft     = secondsLeft % 60;

			secondsLeft += 1;
			if ( secondsLeft == 60 ) {
				minutesLeft += 1;
				secondsLeft = 0;
			} else if ( t_P1 == 0 ) {
				secondsLeft = 0;
			} else if ( t_P1 == time * 60000 ) {
				secondsLeft -= 1;
			}
			if (secondsLeft < 10) {
				p1.setText("" + minutesLeft + ":0" + secondsLeft);
			} else {
				p1.setText("" + minutesLeft + ":" + secondsLeft);
			}
		}

		ImageButton pp = (ImageButton)findViewById(R.id.Pause);
		pp.setImageResource(R.drawable.ic_pause_white_24dp);//pp.setBackgroundColor(Color.LTGRAY);

		/**
		 * Unregister the handler from player 1's clock and
		 * create a new one which we register with this clock.
		 */
		myHandler.removeCallbacks(mUpdateTimeTask);
		myHandler.removeCallbacks(mUpdateTimeTask2);
		myHandler.postDelayed(mUpdateTimeTask2, 100);

		if( notify )
			sendMessage("{ \"tick\":false,\"player\":0, \"time\": \"" + p1.getText().toString() + "\"}");

		return true;
	}

	/** Handles the "tick" event for Player 2's clock */
	private Runnable mUpdateTimeTask2 = new Runnable() {
		public void run() {
			TextView p2 = (TextView)findViewById(R.id.t_Player2);
			String delay_string = "";

			/** Check for delays and apply them */
			if ( delay.equals(FISCHER) && !delayed ) {
				delayed = true;
				t_P2 += delay_time * 1000;
			} else if ( delay.equals(BRONSTEIN) && !delayed ) {
				delayed = true;
				b_delay = delay_time * 1000; //Deduct the first .1s;
				t_P2 += 100; //We'll deduct this again shortly
				delay_string = "+" + ( b_delay / 1000 );
			} else if ( delay.equals(BRONSTEIN) && delayed ) {
				if ( b_delay > 0 ) {
					b_delay -= 100;
					t_P2 += 100;
				}
				if (b_delay > 0 ) {
					delay_string = "+" + ( ( b_delay / 1000 ) + 1 );
				}
			}

			/** Deduct 0.1s from P2's clock */
			t_P2 -= 100;
			long timeLeft = t_P2;

			/** Format for display purposes */
			int secondsLeft = (int) (timeLeft / 1000);
			int minutesLeft = secondsLeft / 60;
			secondsLeft     = secondsLeft % 60;

			secondsLeft += 1;
			if ( secondsLeft == 60 ) {
				minutesLeft += 1;
				secondsLeft = 0;
			} else if ( timeLeft == 0 ) {
				secondsLeft = 0;
			} else if ( timeLeft == time * 60000 ) {
				secondsLeft -= 1;
			}

			/** Did we run out of time? */
			if ( timeLeft == 0 ) {
				timeup = true;
				Button b1 = (Button)findViewById(R.id.Player1);
				Button b2 = (Button)findViewById(R.id.Player2);
				ImageButton pp = (ImageButton)findViewById(R.id.Pause);

				/** Set P1's button and clock text to red */
				p2.setTextColor(Color.RED);
				b1.setBackgroundResource(R.drawable.button_bg_red);
				p2.setOnClickListener(SettingsListener);

				b1.setClickable(false);
				b2.setClickable(false);
				pp.setClickable(false);

				Uri uri = Uri.parse(alertTone);
				ringtone = RingtoneManager.getRingtone(getBaseContext(), uri);
				if ( null != ringtone ) {
					ringtone.play();
				}
				/** Blink the clock display */
				myHandler.removeCallbacks(mUpdateTimeTask2);
				myHandler.postDelayed(Blink2, 500);

				sendMessage("{ \"tick\":true,\"player\":1, \"time\": \"" + "0:00" + "\"}");

				return;
			}

			/** Color clock yellow if we're under 1 minute */
			if ( timeLeft < 60000) {
				p2.setTextColor(Color.rgb(255,150,0));
			} else {
				p2.setTextColor(Color.LTGRAY);
			}

			/** Display the time, omitting leading 0's for times < 10 minutes */
			if (secondsLeft < 10) {
				p2.setText("" + minutesLeft + ":0" + secondsLeft + delay_string);
			} else {
				p2.setText("" + minutesLeft + ":" + secondsLeft + delay_string);
			}

			/** Re-post the handler so it fires in another 0.1s */
			myHandler.postDelayed(this, 100);

			sendMessage("{ \"tick\":true,\"player\":1, \"time\": \"" + p2.getText().toString() + "\"}");
		}
	};

	/** Blinks the clock text if Player 1's time hits 0:00 */
	private Runnable Blink = new Runnable() {
		public void run() {
			TextView p1 = (TextView)findViewById(R.id.t_Player1);

			/**
			 * Display the clock if it's blank, or blank it if
			 * it's currently displayed.
			 */
			if ( !blink ) {
				blink = true;
				p1.setText("");
			} else {
				blink = false;
				p1.setText("0:00");
			}

			/** Register a handler to fire again in 0.5s */
			myHandler.postDelayed(this, 500);
		}
	};

	/** Blinks the clock text if Player 2's time hits 0:00 */
	private Runnable Blink2 = new Runnable() {
		public void run() {
			TextView p2 = (TextView)findViewById(R.id.t_Player2);

			/**
			 * Display the clock if it's blank, or blank it if
			 * it's currently displayed.
			 */
			if ( !blink ) {
				blink = true;
				p2.setText("");
			} else {
				blink = false;
				p2.setText("0:00");
			}

			/** Register a handler to fire again in 0.5s */
			myHandler.postDelayed(this, 500);
		}
	};

	/**
	 * Pauses both clocks. This is called when the options
	 * menu is opened, since the game needs to pause
	 * but not un-pause, whereas PauseToggle() will switch
	 * back and forth between the two.
	 *  */
	private void PauseGame() {
		Button p1 = (Button)findViewById(R.id.Player1);
		Button p2 = (Button)findViewById(R.id.Player2);
		ImageButton pp = (ImageButton)findViewById(R.id.Pause);

		/** Save the currently running clock, then pause */
		if ( ( onTheClock != 0 ) && ( !timeup ) ) {
			savedOTC = onTheClock;
			onTheClock = 0;

			p1.setBackgroundResource(R.drawable.rounded_corners_bg);//p1.setBackgroundColor(Color.LTGRAY);
			p2.setBackgroundResource(R.drawable.rounded_corners_bg);//p2.setBackgroundColor(Color.LTGRAY);
			pp.setImageResource(R.drawable.ic_play_arrow_white_24dp);

			myHandler.removeCallbacks(mUpdateTimeTask);
			myHandler.removeCallbacks(mUpdateTimeTask2);
		}
	}

	/** Called when the pause button is clicked */
	private void PauseToggle() {
		Button p1 = (Button)findViewById(R.id.Player1);
		Button p2 = (Button)findViewById(R.id.Player2);
		ImageButton pp = (ImageButton)findViewById(R.id.Pause);

		pp.performHapticFeedback(1);

		/** Figure out if we need to pause or unpause */
		if ( onTheClock != 0 ) {
			savedOTC = onTheClock;
			onTheClock = 0;

			p1.setBackgroundResource(R.drawable.rounded_corners_bg);//p1.setBackgroundColor(Color.LTGRAY);
			p2.setBackgroundResource(R.drawable.rounded_corners_bg);//p2.setBackgroundColor(Color.LTGRAY);
			pp.setImageResource(R.drawable.ic_play_arrow_white_24dp);

			myHandler.removeCallbacks(mUpdateTimeTask);
			myHandler.removeCallbacks(mUpdateTimeTask2);
		} else {
			//Log.v(TAG, "Info: Unpausing.");
			if ( savedOTC == 1 ) {
				P1Click(false);
			} else if ( savedOTC == 2 ) {
				P2Click(false);
			} else {
				return;
			}
		}
	}

	/** Set up (or refresh) all game parameters */
	private void SetUpGame() {
		/** Load all stored preferences */
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		/** Take care of a haptic change if needed */
		haptic = prefs.getBoolean("prefHaptic", false);

		TextView p1 = (TextView)findViewById(R.id.t_Player1);
		TextView p2 = (TextView)findViewById(R.id.t_Player2);
		p1.setTextColor(Color.LTGRAY);
		p2.setTextColor(Color.LTGRAY);

		Button p1_button = (Button)findViewById(R.id.Player1);
		Button p2_button = (Button)findViewById(R.id.Player2);
		ImageButton pause = (ImageButton)findViewById(R.id.Pause);

		p1_button.setHapticFeedbackEnabled(haptic);
		p2_button.setHapticFeedbackEnabled(haptic);
		pause.setHapticFeedbackEnabled(haptic);

		if (hapticChange)
		{
			/**
			 * We're just changing haptic feedback on this run through,
			 * don't reload everything else!
			 */
			hapticChange = false;
			return;
		}

		delay = prefs.getString("prefDelay","None");
		if ( delay.equals("")) {
			delay = "None";
			SharedPreferences.Editor e = prefs.edit();
			e.putString("prefDelay", "None");
			e.commit();
		}

		try {
			time = Integer.parseInt( prefs.getString("prefTime", "10") );
		} catch (Exception ex) {
			time = 10;
			SharedPreferences.Editor e = prefs.edit();
			e.putString("prefTime", "10");
			e.commit();
		}

		try {
			delay_time = Integer.parseInt( prefs.getString("prefDelayTime", "0") );
		} catch (Exception ex) {
			delay_time = 0;
			SharedPreferences.Editor e = prefs.edit();
			e.putString("prefDelayTime", "0");
			e.commit();
		}

		alertTone = prefs.getString("prefAlertSound", Settings.System.DEFAULT_RINGTONE_URI.toString());
		if (alertTone.equals("")) {
			alertTone = Settings.System.DEFAULT_RINGTONE_URI.toString();
			SharedPreferences.Editor e = prefs.edit();
			e.putString("prefAlertSound", alertTone);
			e.commit();
		}

		Uri uri = Uri.parse(alertTone);
		ringtone = RingtoneManager.getRingtone(getBaseContext(), uri);

		/** Set time equal to minutes * ms per minute */
		t_P1 = time * 60000;
		t_P2 = time * 60000;

		/** Set up the buttons */
		p1_button.setBackgroundResource(R.drawable.rounded_corners_bg);//p1.setBackgroundColor(Color.LTGRAY);
		p2_button.setBackgroundResource(R.drawable.rounded_corners_bg);//p2.setBackgroundColor(Color.LTGRAY);
		pause.setImageResource(R.drawable.ic_play_arrow_white_24dp);

		/** Format and display the clocks */
		p1.setText(FormatTime(t_P1));  p1.setOnClickListener(null);
		p2.setText(FormatTime(t_P2));  p2.setOnClickListener(null);
		/**
		 * Register the click listeners and unregister any
		 * text blinking timers that may exist.
		 */
		p1_button.setOnClickListener(P1ClickHandler);
		p2_button.setOnClickListener(P2ClickHandler);
		pause.setOnClickListener(P2ClickHandler); // Just to begin with. Set to pauseClickListener in P2ClickHandler.
		myHandler.removeCallbacks(Blink);
		myHandler.removeCallbacks(Blink2);

	}




	/**-----------------------------------
	 *         BLUETOOTH RELATED
	 *-----------------------------------*/

	/**
	 * Establish connection with other divice
	 *
	 * @param data   An {@link Intent} with {@link BluetoothDeviceListActivity#EXTRA_DEVICE_ADDRESS} extra.
	 * @param secure Socket Security type - Secure (true) , Insecure (false)
	 */
	private void connectDevice(Intent data, boolean secure) {
		// Get the device MAC address
		String address = data.getExtras()
				.getString(BluetoothDeviceListActivity.EXTRA_DEVICE_ADDRESS);
		// Get the BluetoothDevice object
		BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
		// Attempt to connect to the device
		mBTmessagingService.connect(device, secure);
	}

	/**
	 * Updates the status on the action bar.
	 *
	 * @param resId a string resource ID
	 */
	private static void setStatus(int resId, Activity activity) {

	}

	/**
	 * Updates the status on the action bar.
	 *
	 * @param subTitle status
	 */
	private static void setStatus(CharSequence subTitle, Activity activity) {

	}

	/**
	 * The Handler that gets information back from the BluetoothChatService
	 */
	private static class IncomingHandler extends Handler {
		private final WeakReference<Activity> mActivity;
		String titleConnectTo;

		IncomingHandler(Activity a, String titleConnectTo) {
			mActivity = new WeakReference<Activity>(a);
			this.titleConnectTo = titleConnectTo;
		}
		@Override
		public void handleMessage(Message msg) {
			Activity activity = mActivity.get();
			switch (msg.what) {
				case BluetoothMessagingService.Constants.MESSAGE_STATE_CHANGE:
					switch (msg.arg1) {
						case BluetoothMessagingService.STATE_CONNECTED:
							setStatus(titleConnectTo, activity);
							break;
						case BluetoothMessagingService.STATE_CONNECTING:
							setStatus("connecting...", activity);
							break;
						case BluetoothMessagingService.STATE_LISTEN:
						case BluetoothMessagingService.STATE_NONE:
							setStatus("not connected", activity);
							break;
					}
					break;
				case BluetoothMessagingService.Constants.MESSAGE_WRITE:
					byte[] writeBuf = (byte[]) msg.obj;
					// construct a string from the buffer
					String writeMessage = new String(writeBuf);
					//Toast.makeText(activity, "This:  " + writeMessage, Toast.LENGTH_LONG).show();
					break;
				case BluetoothMessagingService.Constants.MESSAGE_READ:
					byte[] readBuf = (byte[]) msg.obj;
					// construct a string from the valid bytes in the bufferHandler
					String readMessage = new String(readBuf, 0, msg.arg1);
					try {
						JSONObject json = new JSONObject(readMessage);
						int action = json.getInt("action");
						switch (action) {
							case BluetoothMessagingService.Constants.ACTION_CLICK:
								String LANofMv = json.getString("lanStr");
								if ( activity != null) {
									boolean wasPaused = ((ChessClock) activity).onTheClock == 0;
									if (wasPaused)
										((ChessClock) activity).PauseToggle();
									if( ! ((ChessClock) activity).P1Click(false) )
										  ((ChessClock) activity).P2Click(false);
									if( wasPaused )
										((ChessClock) activity).PauseGame();
									Toast.makeText(activity, LANofMv, Toast.LENGTH_LONG).show();
								}
								break;
							case BluetoothMessagingService.Constants.ACTION_PAUSE:
								if ( activity != null )
									((ChessClock) activity).PauseGame();
								break;
							case BluetoothMessagingService.Constants.ACTION_RESUME:
								if ( activity != null ) {
									((ChessClock) activity).PauseGame();
									((ChessClock) activity).PauseToggle();
								}
								break;
							case BluetoothMessagingService.Constants.ACTION_RESET:
								if ( activity != null ) {
									((ChessClock) activity).SetUpGame();
									((ChessClock) activity).onTheClock = 0;
								}
								break;
							default:
								break;
						}

					} catch (JSONException e) {}
					//Toast.makeText(activity, mConnectedDeviceName + ":  " + readMessage, Toast.LENGTH_LONG).show();
					break;
				case BluetoothMessagingService.Constants.MESSAGE_DEVICE_NAME:
					// save the connected device's name
					mConnectedDeviceName = msg.getData().getString(BluetoothMessagingService.Constants.DEVICE_NAME);
					if (null != activity) {
						Toast.makeText(activity, "Connected to "
								+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();
					}
					break;
				case BluetoothMessagingService.Constants.MESSAGE_TOAST:
					if (null != activity) {
						Toast.makeText(activity, msg.getData().getString(BluetoothMessagingService.Constants.TOAST),
								Toast.LENGTH_SHORT).show();
					}
					break;
			}
		}
	}

	private boolean informed = false;
	/**
	 * Sends a message.
	 *
	 * @param message A string of text to send.
	 */
	private void sendMessage(String message) {
		// return if bluetooth is not activated.
		if ( mBTmessagingService == null )
			return;

		// Check that we're actually connected before trying anything
		if (mBTmessagingService.getState() != BluetoothMessagingService.STATE_CONNECTED) {
			//################# sporadic experiment to see if we can "jump start" the connection. #######
			if ( mBTmessagingService.getState() == BluetoothMessagingService.STATE_NONE ) // copied from onResume() method
			{
				mBTmessagingService.start();
			}
			// ################### end of experiment ##############################
			if ( !informed ) {
				Toast.makeText(this, "not connected", Toast.LENGTH_SHORT).show();
				informed = true;
			}
			return;
		}

		// Check that there's actually something to send
		if (message.length() > 0) {
			// Get the message bytes and tell the BluetoothChatService to write
			byte[] send = message.getBytes();
			mBTmessagingService.write(send);

			// Reset out string buffer to zero and clear the edit text field
			mOutStringBuffer.setLength(0);
		}
	}

	/**
	 * Set up the UI and background operations for chat.
	 */
	private void setupComLink() {
		//Log.d(TAG, "setupComLink()");

		// Initialize the BluetoothMessagingService to perform bluetooth connections
		String arg1 = getString(R.string.none_found, mConnectedDeviceName);
		mBTmessagingService = new BluetoothMessagingService( this, new IncomingHandler(this, arg1));

		// Initialize the buffer for outgoing messages
		mOutStringBuffer = new StringBuffer("");
	}
}
