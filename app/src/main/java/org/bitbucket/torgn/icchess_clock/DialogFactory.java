/*************************************************************************
 * File: DialogFactory.java
 * 
 * Creates the 'About' dialog.
 * 
 * Created: 7/3/2010
 * 
 * Author: Carter Dewey
 *
 * Modified: 1/13/2016 by Tor Garmann Naerland
 *************************************************************************
 *
 *   This file is part of Simple Chess Clock (ICCC).
 *    
 *   ICCC is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ICCC is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with ICCC.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/
package org.bitbucket.torgn.icchess_clock;

import android.app.Dialog;
import android.content.Context;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class DialogFactory {
	public DialogFactory() {
		
	}
	
	public Dialog AboutDialog(Context c, String MAJOR, String MINOR, String MINI) {
		Dialog d = new Dialog(c);
		
		d.setContentView(R.layout.about_dialog);
		d.setTitle("About ICChess Clock");

		TextView text = (TextView) d.findViewById(R.id.text);
		text.setText( d.getContext().getText(R.string.about_text) );
		text.setMovementMethod(LinkMovementMethod.getInstance());  // to make the link clickable
		
		return d;
	}

}
